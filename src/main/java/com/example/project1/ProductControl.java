package com.example.project1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
public class ProductControl {
    @Autowired ProductService service;
    @GetMapping("/listproduct")
    public String showListProduct(Model model){
        List<Product> productList=service.productList();
        model.addAttribute("productList",productList);
        return "listproduct";
    }
    @GetMapping("/listproduct/addProduct")
    public String AddProduct(Model model){
        model.addAttribute("product",new Product());
        return "addProduct";
    }
    @PostMapping("/product/save")
    public String saveProduct(Product product){
        service.save(product);
        return "redirect:/listproduct";
    }
    @GetMapping("/listproduct/edit/{idpro}")
    public String showEdit(@PathVariable("idpro") Integer idpro , Model model){
        try{
            Product product = service.get(idpro);
            model.addAttribute("product", product);
            return "addProduct";
        }catch(ProductNotFoundException e){
            return "redirect:/listproduct";
        }
    }
    @GetMapping("/listproduct/delete/{idpro}")
    public String deleteProduct(@PathVariable("idpro") Integer idpro){
        service.delete(idpro);
        return "redirect:/listproduct";
    }
    @GetMapping("/detail/{idpro}")
    public String detailProduct(@PathVariable("idpro") Integer idpro, Model model){

        Product p =service.getid(idpro);
        model.addAttribute("product",p);
        return "detail";
    }

}
