package com.example.project1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class HomeController {

    @Autowired private ProductService service;
    @GetMapping("")
    public String show(Model model){
        List<Product> productList=service.productList();
        model.addAttribute("home",productList);
        return "index";
    }


}
