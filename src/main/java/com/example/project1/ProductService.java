package com.example.project1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class ProductService {
    @Autowired private ProductRepository repo;
    public List<Product> productList(){
        return(List<Product>) repo.findAll();
    }
    public void save(Product product){
        repo.save(product);
    }
    public Product get(Integer idpro) throws ProductNotFoundException {
       Optional<Product> result = repo.findById(idpro);
       if(result.isPresent()){
            return result.get();
       }
       throw new ProductNotFoundException("Could not find");
    }
    public Product getid(Integer idpro){
        Optional<Product> product = repo.findById(idpro);
        return product.get();
    }
    public void delete(Integer idpro){
        repo.deleteById(idpro);
    }
}
